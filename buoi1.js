/**
 * Bài 1 : Tính Tiền Lương Nhân Viên
 * input :- Số tiền 1 ngày
 *        - số ngày làm
 * step
 * + tạo 2  biến : số ngày lam và số tiền 1 ngày
 * + tạo biến kết quả : tiền lương
 * + tình tiền lương = Số tiền 1 ngày * số ngày làm
 * output :
 * in kết quả :console.log("tienLuong: ", tienLuong);
 */
var tien = 100000;
var soNgay = 30;
var tienLuong = null;
tienLuong = tien * soNgay;
console.log("tienLuong: ", tienLuong);

/**
 * Bài 2 Tính Giá trị Trung Bình
 * input :
 *  - nhập vào 5 số thực
 * step
 *  - tạo biến cho 5 số thực
 *  - tạo biến kết quả : giá trị trung bình (gttb)
 * tính gttb = (num1 +num2 +num3 +num4 +num5 )/5
 * output
 *  in kết quả :  console.log('gttb: ', gttb);
 */
var num1 = 3;
var num2 = 4;
var num3 = 5;
var num4 = 6;
var num5 = 7;
var gttb = null;
gttb = (num1 + num2 + num3 + num4 + num5) / 5;
console.log("gttb: ", gttb);

/**
 * Bài 3 : quy dổi tiền
 * input :
 * - giá tiền 1 usd
 * - số usd cần quy đổi
 * step
 * - tạo biến giá 1 usd = 23.500vnd
 * - tạo biến số usd cần quy đổi : so_usd
 * - tạo biến kết quả: tien_vnd
 * - tien_vdn = so_usd * 23.5000
 * output
 * console.log('tien_vdn: ', tien_vdn);
 *
 */
var giaUsd = 23500;
var soUsd = 10;
var tienVnd = null;
tienVnd = giaUsd * so_usd;
console.log("tien_vnd: ", tienVnd);

/**   bài 4 
 * input: độ dài hai canh hcn
 * *step :
 - b1 tạo hai biến chưa hai cạnh hcn
 - b2 tạo biến chưa kq dien tich và chu vi 
 - b3 ap dung ct tính
 + diện tích = dai * rộng;
 + chu vi = (dai + rong)*2
 * output:
 in kết qua:
  dienTich
 console.log('dienTich: ', dienTich);
 chuVi
 console.log('chuVi: ', chuVi);
 
 */
var dai = 10;
var rong = 5;
var dienTich = null;
var chuVi = null;
dienTich = dai * rong;
console.log("dienTich: ", dienTich);
chuVi = (dai + rong) * 2;
console.log("chuVi: ", chuVi);

/**
 * Bài 5: Tính tổng 2 ký số
 * input :
 * - nhập 1 số có 2 chữ số
 * step
 * + tạo biến số cần tính tổng ký số : number
 * + tạo biến kết quả  : result
 * + tính tổng ký số
 * - hàng đơn vị : donvi  =  number % 10 (chia dư cho 10 lấy phần dư )
 * - hàng chục:    chuc =  Math.floor(number / 10) ;
 * - kết quả : result = donvi + chuc ;
 * output
 * in kết quả
 * console.log('result: ', result);
 */
var number = 83;
var donvi = number % 10; //3
var chuc = Math.floor(number / 10); //8

var result = donvi + chuc;
console.log("result: ", result);
